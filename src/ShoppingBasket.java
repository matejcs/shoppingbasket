public interface ShoppingBasket {


    /**
     * Calculates the total cost of the shopping items in the shopping basket.
     * @param items represents the list of items in the shopping basket.
     * @return total cost of each of the items in the basket. 0 if no items specified.
     */
    int calculateTotalCost(String[] items);

}
