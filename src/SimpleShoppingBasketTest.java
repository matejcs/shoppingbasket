import static org.junit.Assert.*;

public class SimpleShoppingBasketTest {

    private final ShoppingBasket shoppingBasket = new SimpleShoppingBasket();

    @org.junit.Test
    public void calculateTotalCostForNullShoppingList() {
        assertEquals(0, shoppingBasket.calculateTotalCost(null));
    }

    @org.junit.Test
    public void calculateTotalCostForEmptyShoppingList() {
        assertEquals(0, shoppingBasket.calculateTotalCost(new String[]{}));
    }

    @org.junit.Test
    public void calculateTotalCostForOneApple() {
        assertEquals(35, shoppingBasket.calculateTotalCost(new String[]{"Apple"}));
    }

    @org.junit.Test
    public void calculateTotalCostForTwoApples() {
        assertEquals(70, shoppingBasket.calculateTotalCost(new String[]{"Apple", "Apple"}));
    }

    @org.junit.Test
    public void calculateTotalCostForOneBanana() {
        assertEquals(20, shoppingBasket.calculateTotalCost(new String[]{"Banana"}));
    }

    @org.junit.Test
    public void calculateTotalCostForTwoBananas() {
        assertEquals(40, shoppingBasket.calculateTotalCost(new String[]{"Banana", "Banana"}));
    }

    @org.junit.Test
    public void calculateTotalCostForOneAppleOneBanana() {
        assertEquals(55, shoppingBasket.calculateTotalCost(new String[]{"Apple", "Banana"}));
    }

    @org.junit.Test
    public void calculateTotalCostForOneMelon() {
        assertEquals(50, shoppingBasket.calculateTotalCost(new String[]{"Melon"}));
    }

    @org.junit.Test
    public void calculateTotalCostForTwoMelons() {
        assertEquals(50, shoppingBasket.calculateTotalCost(new String[]{"Melon", "Melon"}));
    }

    @org.junit.Test
    public void calculateTotalCostForThreeMelons() {
        assertEquals(100, shoppingBasket.calculateTotalCost(new String[]{"Melon", "Melon", "Melon"}));
    }

    @org.junit.Test
    public void calculateTotalCostForFourMelons() {
        assertEquals(100, shoppingBasket.calculateTotalCost(new String[]{"Melon", "Melon", "Melon", "Melon"}));
    }

    @org.junit.Test
    public void calculateTotalCostForOneLime() {
        assertEquals(15, shoppingBasket.calculateTotalCost(new String[]{"Lime"}));
    }

    @org.junit.Test
    public void calculateTotalCostForTwoLimes() {
        assertEquals(30, shoppingBasket.calculateTotalCost(new String[]{"Lime", "Lime"}));
    }

    @org.junit.Test
    public void calculateTotalCostForThreeLimes() {
        assertEquals(30, shoppingBasket.calculateTotalCost(new String[]{"Lime", "Lime", "Lime"}));
    }

    @org.junit.Test
    public void calculateTotalCostForFourLimes() {
        assertEquals(45, shoppingBasket.calculateTotalCost(new String[]{"Lime", "Lime", "Lime", "Lime"}));
    }

}