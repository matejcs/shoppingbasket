public class SimpleShoppingBasket implements ShoppingBasket {

    /**
     * Naive implementation for calculating the total cost of the shopping basket.
     * Items are priced as follows:
     *  - Apples are 35p each
     *  - Bananas are 20p each
     *  - Melons are 50p each, but are available as �buy one get one free�
     *  - Limes are 15p each, but are available in a �three for the price of two� offer
     * @param items represents the list of items in the shopping basket.
     * @return total cost of each item in the basket while considering the price rules.
     */
    @Override
    public int calculateTotalCost(String[] items) {
        if (items == null || items.length == 0) {
            return 0;
        }

        int totalCost = 0;
        int melonCount = 0;
        int limeCount = 0;

        for (String item: items) {
            if ("Apple".equals(item)) {
                totalCost += 35;
            }

            if ("Banana".equals(item)) {
                totalCost += 20;
            }

            if ("Melon".equals(item)) {
                ++melonCount;
            }

            if ("Lime".equals(item)) {
                ++limeCount;
            }
        }

        totalCost += (melonCount / 2) * 50;
        if (melonCount % 2 == 1) {
            totalCost += 50;
        }

        totalCost += (limeCount / 3) * 30;
        if (limeCount % 3 != 0) {
            totalCost += (limeCount % 3) * 15;
        }

        return totalCost;
    }

}
